/*
 * File:   GameObject.cpp
 * Created on April 10, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * GameObject methods
 */

#include "GameObject.h"

int GameObject::getX() const {
    return this->x;
}

int GameObject::getY() const {
    return this->y;
}

float GameObject::getSize() const {
    return this->size;
}

int GameObject::getID() const {
    return this->ID;
}


Player* GameObject::getOwner() {
		return owner;
}

int GameObject::setDamage(int damage) {
    this->currentLife -= damage;
    if(currentLife <= 0 ) {
        dead = true;
    }
    return currentLife;
}

int GameObject::getLife() const {
    return this->life;
}

int GameObject::getCurrentLife() const {
    return this->currentLife;
}
