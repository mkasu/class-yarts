/*
 * File:   Audio.cpp
 * Created on May 1, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Audio class
 */
#include <string>
#include <ctime>
#include <cstdlib>

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef __APPLE__
#include <SDL/SDL.h>
//#include <SDL_mixer/SDL_mixer.h>
#else
#include <SDL/SDL.h>
//#include <SDL/SDL_mixer.h>
#endif

#include "Audio.h"

static Audio* instance;

Audio::Audio() {
	music = NULL;
    instance = this;
    //Mix_OpenAudio( 44010, MIX_DEFAULT_FORMAT, 2, 4096 );
    //Load the music
    //loadMusic();
    //Mix_HookMusicFinished(musicDone);
}

void Audio::loadMusic() {
    srand ( time(NULL) );
    int id = rand() % 8;
    std::string musicFile = "audio/music/";
    switch(id) {
        case 0:
            musicFile.append("bell.mp3");
            break;
        case 1:
            musicFile.append("hwadu.mp3");
            break;
        case 2:
            musicFile.append("iamone.mp3");
            break;
        case 3:
            musicFile.append("iwycswis.mp3");
            break;
        case 4:
            musicFile.append("mtit.mp3");
            break;
        case 5:
            musicFile.append("TheEtherLife.mp3");
            break;
        case 6:
            musicFile.append("uhohp2.mp3");
            break;
        case 7:
            musicFile.append("wiit.mp3");
            break;
    }
    //music = Mix_LoadMUS( musicFile.c_str() );
    //Mix_PlayMusic( music, -1 );
    //Mix_VolumeMusic(MIX_MAX_VOLUME/4);
}

Mix_Music* Audio::getMusic() {
    return this->music;
}

void musicDone() {
    //Mix_HaltMusic();
    //Mix_FreeMusic(instance->getMusic());
    //instance->loadMusic();
}

