/*
 * File:   GameState.h
 * Created on April 22, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definition of GameState, state of the game object having
 * pointers to everything.
 */

#ifndef GAMESTATE_H
#define	GAMESTATE_H

#include <vector>
#include <string>

#include "Data.h"
#include "Settings.h"

class Player;
class Building;
class Unit;
class Ressources;
class Map;
class Timer;
class Tooltip;
class SDL_Surface;
class GLRenderer;
class FogOfWar;

/**
 * This class does a lot of communication between classes
 * Even though circular class models aren't the best, this
 * game state object does a pretty good job and makes a lot
 * of things easier.
 *
 * It contains basically a pointer to each important array, vector
 * or object which is created in Game class, so I can access them
 * from other classes to ask for existing Maps, Buildings, Units, etc.
 */
class GameState {
public:
    /**
     * Constructor. Gets called from Game class and gives GameState
     * object a pointer to everything important.
     *
     * @param m pointer to map of game
     * @param b pointer to vector of pointers to buildings
     * @param u pointer to vector of pointers to units
     * @param a pointer to starttime for announcements
     * @param t pointer to timer
     * @param aS pointer to announcement texts
     * @param p pointer to vector of pointers to players
     * @param to pointer to tooltip which is currently displayed
     * @param fov pointer to fog of war object
     */
    GameState(Map* m, std::vector<Building*>* b,
    		  std::vector<Unit*>* u, int* a, Timer* t,
    		  std::string* aS, std::vector<Player*>* p,
    		  Tooltip* to, FogOfWar* fov);

    /**
     * Destructor
     */
    ~GameState();

    /**
     * Get pointer to vector of players
     *
     * @return pointer
     */
    std::vector<Player*>* getPlayers();

    /**
     * Get pointer to map
     *
     * @return pointer
     */
    Map* getMap();

    /**
     * Get pointer to vector of buildings
     *
     * @return pointer
     */
    std::vector<Building*>* getBuildings();

    /**
     * Get pointer to vector of units
     *
     * @return pointer
     */
    std::vector<Unit*>*  getUnits();

    /**
     * Get pointer to timer
     *
     * @return pointer
     */
    Timer* getTimer();

    /**
     * Starting announcement
     */
    void updateAnnounce();

    /**
     * Setting announcement text
     *
     * @param announceText text which should displayed
     */
    void setAnnounceText(std::string announceText);

    /**
     * Getting current active Tooltip
     *
     * @return pointer active Tooltip
     */
    Tooltip* getActiveTooltip();
     /**
     * Setting current active Tooltip
     *
     * @param t new tooltip to set to
     */
     void setActiveTooltip(Tooltip* t);

     /**
      * Getting data object
      *
      * @return data object
      */
     Data* getData();

     /**
      * Getting screen to draw on
      *
      * @return screen object
      */
     SDL_Surface* getScreen();
     /**
      * Setting screen device
      *
      * @param s surface to set screen to
      */
     void setScreen(SDL_Surface* s);
     /**
      * Setting pointer to renderer object
      * 
      * @param r renderer object
      */
     void setRenderer(GLRenderer* r);

     /**
      * Getting pointer to renderer object
      *
      * @return renderer object
      */
     GLRenderer* getRenderer();

     /**
      * Gets pointer to fog of war object
      *
      * @return fog of war object
      */
     FogOfWar* getFOV();

     /**
      * Gets settings values
      */
	template < typename T >
	T getValue(std::string key) {
		return settings->getValue<T>(key);
	}

private:
    /**
     * Screen where SDL is drawing on
     */
    SDL_Surface* screen;

    /**
     * Settings object, hold configurations of the game.
     */
    Settings* settings;

    /**
     * Fog of war object
     */
    FogOfWar* fov;

    /**
     * Data object for whole game
     */
    Data data;

    /**
     * Pointer to games map
     */
    Map* map;

    /**
     * Pointer to vector of games buildings
     */
    std::vector<Building*>* buildings;

    /**
     * Pointer to vector of games units
     */
    std::vector<Unit*>*  units;

    /**
     * Pointer to vector of games players
     */
    std::vector<Player*>* players;

    /**
     * Start time of announcements
     */
    int* announce;

    /**
     * A timer parallel to game loop
     */
    Timer* timer;

    /**
     * Pointer to announcement text
     */
    std::string* announceText;

    /**
     * Pointer to current active Tooltip
     */
    Tooltip* activeTooltip;

    /**
     * Pointer to renderer object
     */
    GLRenderer* renderer;
};

#endif	/* GAMESTATE_H */

