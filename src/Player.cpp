/*
 * File:   Player.cpp
 * Created on April 25, 2011
 * 
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Player methods
 */

#include "Player.h"

Player::Player(int i) {
	// Init of color, should be overwritten by setColor(), but in case
	// it won't, it's more secure to init this array.
	this->color[0] = 0.0f;
	this->color[1] = 0.0f;
	this->color[2] = 0.0f;

	this->id = i;
    this->ressources = new Ressources(this);
}

Player::~Player() {
	delete(ressources);
}

int Player::getID() const {
    return id;
}

Ressources* Player::getRessources() {
    return this->ressources;
}

void Player::setColor(float r, float g, float b) {
    this->color[0] = r;
    this->color[1] = g;
    this->color[2] = b;
}

float* Player::getColor() {
    return color;
}
