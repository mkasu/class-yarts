/*
 * File:   Tile.cpp
 * Created on April 6, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Tiles methods
 */
#include "Tile.h"

void Tile::setUsed(bool toggle) {
    this->used = toggle;
}

float* Tile::getMColor() {
    return this->mColor;
}

int Tile::getSpeed() const {
    return this->speed;
}

bool Tile::getUsed() const {
    return this->used;
}

int Tile::getType() const {
    return this->type;
}

int Tile::getX() const {
    return this->x;
}

int Tile::getY() const {
    return this->y;
}
int Tile::getID() const {
    return this->ID;
}

TilesGrass::TilesGrass(int x, int y) {
    this->x = x;
    this->y = y;
    
    this->speed = 1;
    this->type = 1;
    this->used = false;
    this->ID = 0;
    this->mColor[0] = 0.0f;
    this->mColor[1] = 0.6f;
    this->mColor[2] = 0.0f;
}

TilesWater::TilesWater(int x, int y) {
    this->x = x;
    this->y = y;
    
    this->speed = 0;
    this->type = 2;
    this->used = false;
    this->ID = 1;
    this->mColor[0] = 0.0f;
    this->mColor[1] = 0.0f;
    this->mColor[2] = 0.7f;
}

TilesPineWoods::TilesPineWoods(int x, int y) {
    this->x = x;
    this->y = y;

    this->speed = 0;
    this->type = 3;
    this->used = false;
    this->ID = 2;
    this->mColor[0] = 0.0f;
    this->mColor[1] = 0.4f;
    this->mColor[2] = 0.0f;
}
