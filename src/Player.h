/*
 * File:   Player.h
 * Created on April 25, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definitions for Player class
 */

#ifndef PLAYER_H
#define	PLAYER_H

#include <vector>

#include "Ressources.h"

/**
 * This class represents a single user. Managing stuff which
 * is different for different players. Like the color their
 * buildings have or the ressources they have.
 */
class Player {
public:
    /**
     * Constructor, initializing ressources
     *
     * @param i id this player gets
     */
    Player(int i);

    /**
     * Destructor, does nothing
     */
    virtual ~Player();

    /**
     * Setting the color a player has got
     *
     * @param r red color
     * @param g blue color
     * @param b green color
     */
    void setColor(float r, float g, float b);

    /**
     * Getting the players color
     * 
     * @return vector of colors
     */
    float* getColor();

    /**
     * Getting pointer to players ressources object
     *
     * @return pointer to Ressources
     */
    Ressources* getRessources();

    /**
     * Get player id
     *
     * @return int
     */
    int getID() const;
private:
    /**
     * ID of this player
     */
    int id;
    
    /**
     * Vector which saves data for all color codes
     */
    float color[3];

    /**
     * Ressources object
     */
    Ressources* ressources;
};

#endif	/* PLAYER_H */

