/*
 * File:   GameState.cpp
 * Created on April 22, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * GameState methods
 */
#ifdef _WIN32
#include <windows.h>
#endif

#include <SDL/SDL.h>

#include "GameState.h"
#include "Building.h"
#include "Unit.h"
#include "Ressources.h"
#include "Map.h"
#include "Timer.h"
#include "GLRenderer.h"
#include "FogOfWar.h"

GameState::GameState(Map* m, std::vector<Building*>* b, std::vector<Unit*>* u, int* a, Timer* t, std::string* aS, std::vector<Player*>* p, Tooltip* to, FogOfWar* fov) {
    this->screen = NULL;
    this->renderer = NULL;

    this->fov = fov;
	this->map = m;
    this->buildings = b;
    this->units = u;
    this->players = p;
    this->announce = a;
    this->timer = t;
    this->announceText = aS;
    this->activeTooltip = to;

    this->settings = new Settings();
}

GameState::~GameState() {
	delete(settings);

	if(renderer != NULL)
		delete(renderer);
}

void GameState::setScreen(SDL_Surface* s) {
    this->screen = s;
}

void GameState::setRenderer(GLRenderer* r) {
    this->renderer = r;
}

GLRenderer* GameState::getRenderer() {
    return this->renderer;
}

SDL_Surface* GameState::getScreen() {
    return this->screen;
}

FogOfWar* GameState::getFOV() {
	return this->fov;
}

Tooltip* GameState::getActiveTooltip() {
    return activeTooltip;
}

void GameState::setActiveTooltip(Tooltip* t) {
    this->activeTooltip = t;
}

Map* GameState::getMap() {
    return map;
}

Data* GameState::getData() {
    return &this->data;
}

std::vector<Building*>* GameState::getBuildings() {
    return buildings;
}

std::vector<Unit*>* GameState::getUnits() {
    return units;
}

std::vector<Player*>* GameState::getPlayers() {
    return players;
}

Timer* GameState::getTimer() {
    return timer;
}

void GameState::updateAnnounce() {
    (*this->announce) = (*timer).get_ticks();
}

void GameState::setAnnounceText(std::string announceText) {
    (*this->announceText) = announceText;
}
