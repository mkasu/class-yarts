/*
 * File:   GameObject.h
 * Created on April 10, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definition of GameObject, parent class of Units and Buildings
 */

#ifndef _GameObject_h_
#define _GameObject_h_

class Player;

/**
 * Parent class for Units and Buildings
 */
class GameObject {
protected:
    /**
     * Position where the object is at
     */
    int x,y;
    
    /**
     * The size of building. Relative to a tile (e.g. 1 = 64, 0.5 = 32)
     */
    float size;

    /**
     * ID of object, more about IDs here http://redmine.mkasu.org/documents/1
     */
    int ID;

    /**
     * Player who's owning this building
     */
    Player* owner;

    /**
     * The maximum life this building has got.
     */
    int life;

    /**
     * The current life a building has, after getting damage or something else.
     */
    int currentLife;

    /**
     * Tracking if object is "dead", that's when it's at 0 hp, indicating
     * for other classes that it can't be used anymore.
     */
    bool dead;
    
public:
    /**
     * Get x coordinate of position of object
     *
     * @return int
     */
    int getX() const;

    /**
     * Get y coordinate of position of object
     *
     * @return int
     */
    int getY() const;

    /**
     * Get size of the object
     *
     * @return int
     */
    float getSize() const;
    
    /**
     * Get ID of object
     *
     * @return int
     */
    int getID() const;

    /**
     * Get maximum life of object.
     * @return maximum life
     */
    int getLife() const;

    /**
     * Doing damage to this object
     * @param damage how much damage you did
     * @return life remaining
     */
    int setDamage(int damage);

    /**
     * Get current life the building has after getting damage (or not).
     * @return current life
     */
    int getCurrentLife() const;

    /**
     * Return pointer to owner of this building
     *
     * @return owner
     */
    Player* getOwner();
};

#endif /* _GameObject_h_ */
