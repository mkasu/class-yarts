/*
 * File:   Data.h
 * Created on April 30, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definition of Data class
 */

#ifndef DATA_H
#define	DATA_H

#include <vector>
#include <string>

/**
 * This class contains most of the game data.
 */
class Data {
public:
	/**
	 * Constructor, filling object with data.
	 */
    Data();

    /**
     * Gives back gold price of object
     *
     * @param id id of object
     * @return int
     */
    int getGoldPrices(int id) const;

    /**
     * Gives back gas price of object
     *
     * @param id id of object
     * @return int
     */
    int getGasPrices(int id) const;

    /**
     * Gives back supply of object. Either how much it gives (buildings) or how
     * much they cost (units).
     *
     * @param id id of object
     * @return int
     */
    int getSupply(int id) const;

    /**
     * Gives back build time of object
     *
     * @param id id of object
     * @return int
     */
    int getBuildTimes(int id) const;

    /**
     * Gives back maximum life of object
     *
     * @param id id of object
     * @return int
     */
    int getLife(int id) const;

    /**
     * Gives back relative (compared to tile size) size of object
     *
     * @param id id of object
     * @return int
     */
    float getSize(int id) const;

    /**
     * Gives back name of object
     *
     * @param id id of object
     * @return string
     */
    std::string getName(int id) const;

    /**
     * Gives back description texts for tooltips
     *
     * @param id id of object
     * @param n line of description text
     * @return string
     */
    std::string getDescription(int id, int n) const;
private:
    /**
     * This vector saves the names of an id
     */
    std::vector<std::string> name;

    /**
     * This vector saves the maximum life of the id
     */
    std::vector<int> life;

    /**
     * This vector saves the size of the id
     */
    std::vector<float> size;

    /**
     * This vector saves the description (for tooltip) of an id
     */
    std::vector<std::vector<std::string> > description;

    /**
     * This vector saves the gold prices of an id
     */
    std::vector<int> goldPrices;

    /**
     * This vector saves the gas prices of an id
     */
    std::vector<int> gasPrices;

    /**
     * This vector saves the supply of an id
     */
    std::vector<int> supplyCost;

    /**
     * This vector saves the build times of an id
     */
    std::vector<int> buildTimes;
};

#endif	/* DATA_H */

