/*
 * File:   UnitGroup.h
 * Created on May 10, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definition of UnitGroup class
 */

#ifndef _UNITGROUP_H_
#define _UNITGROUP_H_

#include <vector>

class Unit;

/**
 * This class will be used to control a group of units, so
 * pathfinding algorithms aren't needed to be called per unit.
 */
class UnitGroup {
private:
	/**
	 * Vector this unit group consists of
	 */
	std::vector<Unit*> units;

public:
	/**
	 * Constructor initializing unit group
	 *
	 * @param units vector of units this unitgroup consists of
	 */
	UnitGroup(std::vector<Unit*>);

	/**
	 * Destructor, not used yet
	 */
	virtual ~UnitGroup();
};

#endif /* _UNITGROUP_H_ */
