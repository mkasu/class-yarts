/*
 * File:   Particle.h
 * Created on May 2, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definitions for Particle class
 */

#ifndef PARTICLE_H
#define	PARTICLE_H

class GameState;
class SDL_Surface;

/**
 * Used to make particles, very very simple "particle" engine
 */
class Particle {
public:
    /**
     * Constructor, loads particle image and randomize start offset
     * 
     * @param x x coordinate start
     * @param y y coordinate start
     * @param xDest x coordinate destination
     * @param yDest y coordinate destination
     * @param gs state of the game object
     */
    Particle(int x,int y,int xDest,int yDest, GameState* gs);

    /**
     * Destructor
     */
    virtual ~Particle();


    /**
     * Gets called every frame. Moves particles.
     */
    void show();

    /**
     * Checks if particle is at destination
     *
     * @return bool
     */
    bool isDead();
private:
    /**
     * State of the game object
     */
    GameState* gameState;

    /**
     * Position x
     */
    int x;

    /**
     * Position y
     */
    int y;

    /**
     * Destination x
     */
    int xDest;

    /**
     * Destination y
     */
    int yDest;
};

#endif	/* PARTICLE_H */

