/*
 * File:   Timer.h
 * Created on April 8, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definition for Timer class
 */

#ifndef _Timer_h_
#define _Timer_h_

/**
 * This class creates a startable, stoppable and pausable timer
 * which runs in another thread along with the games.
 */
class Timer
{
private:
    /**
     * Time when the timer starts
     */
    float startTicks;

    /**
     * Time when the timer gets paused
     */
    float pausedTicks;

    /**
     * Status of the timer, if it is paused
     */
    bool paused;

    /**
     * Status of the timer, if it is started
     */
    bool started;

public:
    /**
     * Constructor
     * does nothing beside initializing attributes with
     * (int)zero or (bool)false.
     */
    Timer();

    /**
     * Start the Timer
     */
    void start();

    /**
     * Stop the timer
     */
    void stop();
    
    /**
     * Pause the timer
     */
    void pause();

    /**
     * Unpause the timer
     */
    void unpause();

    /**
     * Get current time of the timer.
     *
     * @return current Time
     */
    float get_ticks() const;
};

#endif /* _Timer_h_ */
