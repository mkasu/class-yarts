/*
 * File:   Audio.h
 * Created on May 1, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * Definition Audio class
 */

#ifndef AUDIO_H
#define	AUDIO_H

class Mix_Music;

/**
 * Starts a new track. This gets called by the Hook when a music track is
 * done to start a new run.
 */
void musicDone();

/**
 * Handles audio component of game engine.
 */
class Audio {
public:
    /**
     * Initializing sound and starting first music track
     */
    Audio();

    /**
     * Loads a random music file and plays it
     */
    void loadMusic();

    /**
     * Getting a pointer to the current music track
     *
     * @return music track as object
    */
    Mix_Music* getMusic();
private:
    /**
     * Current music track as object
    */
    Mix_Music *music;
};

#endif	/* AUDIO_H */
