/*
 * File:   UnitGroup.cpp
 * Created on May 10, 2011
 *
 * Copyright (C) 2011 by Marc A. Kastner <m.kastner@tu-bs.de>
 * Part of the YaRTS Project http://yarts.mkasu.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * See the COPYING file for more details.
 */

/**
 * @file
 * UnitGroup methods
 */
#include <cmath>

#include "UnitGroup.h"
#include "Unit.h"
#include "GameObject.h"

UnitGroup::UnitGroup(std::vector<Unit*> units) {
	this->units = units;

	float distance = 0.0f;

	for(unsigned int i=0; i<units.size(); i++) {
		int coord = units[i]->getX() - units[i]->getY();
		distance += coord*coord;
	}
	distance = std::sqrt(distance);
}

UnitGroup::~UnitGroup() {
	// TODO Auto-generated destructor stub
}
