# yarts v0.0

A 2D RTS clone, started as an education project for university. 

- Support: <http://yarts.mkasu.org>
- Mainline source code: <https://github.com/mkasu/yarts>
- Twitter: [@mkasu](http://twitter.com/mkasu)

## Build and run

**Get the source:**

    git clone --recursive https://github.com/mkasu/yarts.git
    git remote add upstream https://github.com/mkasu/yarts.git

**Build:**

	cd yarts
	cmake .
	make

**Run:**

	./yarts

## Development

Currently, this project is being developed as an education project for lecture [PADI](http://www.cg.tu-bs.de/teaching/seminars/ss11/padi/) at university
TU Braunschweig. Thats why, I won't accept patches or help until mid of July. After that, you are
welcome to support me.

## License

This project is licensed under GNU Public License Version 3. To read more about this topic, go to
`COPYING`.

## Contribution

Special thanks to following persons contributing artwork and sounds:

- Julian Saraceni (<http://ainotenshi.org>, Textures, UI)
- alienbomb (<http://freesound.org>, Sounds)
